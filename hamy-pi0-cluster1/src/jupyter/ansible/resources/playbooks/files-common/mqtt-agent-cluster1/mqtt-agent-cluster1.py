# -*- coding: utf-8 -*-
#
####################################
# MQTT Agent for hamy-pi0-cluster1 #
####################################
#
# Store this Python 3 source in /etc/mqtt-agent-cluster1/mqtt-agent-cluster1.py
# Permissions: 644

import paho.mqtt.client  
import time
import sys
import blinkt
import logging
import logging.handlers
import random

PROGRAM_NAME = "mqtt-agent-cluster1"
PROGRAM_VERSION = "1.0.0"
TOPIC_ROOT_PART = "cluster1"
TOPIC_8LEDS_PART = "8leds"
TOPICS_FOR_SUBSCRIPTION = [("/" + TOPIC_ROOT_PART + "/" + TOPIC_8LEDS_PART + "/#", 0)]
LOCALHOST = "localhost"
 
logging.basicConfig(level=logging.DEBUG)
rootLogger = logging.getLogger("")
sysLogHandler = logging.handlers.SysLogHandler(address='/dev/log')
rootLogger.addHandler(sysLogHandler)

def text2int(text):
    if not text:
        return 0
    try:
        return int(text)
    except:
        return 0
    
def text2intClamp(text, min, max):
    result = text2int(text)
    if result < min:
        return min
    if result > max:
        return max
    return result

text2boolMap = { "f": False, "t": True, "false": False, "true": True,
                       "off": False, "on" :True, "no": False, "yes":True, "0": False, "1":True }
    
def text2bool(text):
    if not text:
        text = ""
    return text2boolMap.get(text, False)

def wsConcat(*args):
    sep = ""
    result = ""
    for arg in args:
        result += sep
        sep = " "
        result += str(arg) 
    return result

def text2float(text):
    if not text:
        return 0.0
    try:
        return float(text)
    except:
        return 0.0
    
def text2floatClamp(text, min, max):
    result = text2float(text)
    if result < min:
        return min
    if result > max:
        return max
    return result
   

class LoggableObject:
     
    def _computeMessage(self, methodName, *args):
        result = "«" + self.simpleClassName + ":" + methodName + "» " 
        for arg in args:
            result += str(arg)
        return result
   
    def debug(self, methodName, *args):
        if self._logger.isEnabledFor(logging.DEBUG):
            self._logger.debug("%s", self._computeMessage(methodName, *args))
        return methodName
    
    def info(self, methodName, *args):
        if self._logger.isEnabledFor(logging.INFO):
            self._logger.info("%s", self._computeMessage(methodName, *args))
        return methodName
    
    def warning(self, methodName, *args):
        if self._logger.isEnabledFor(logging.WARNING):
            self._logger.warning("%s", self._computeMessage(methodName, *args))
        return methodName
    
    def error(self, methodName, *args):
        if self._logger.isEnabledFor(logging.ERROR):
            self._logger.error("%s", self._computeMessage(methodName, *args))
        return methodName
    
    def __init__(self):
        self.simpleClassName = self.__class__.__qualname__.split(".")[-1]
        self._logger = logging.getLogger(self.simpleClassName)

class Main(LoggableObject):
    
    def __init__(self):
        LoggableObject.__init__(self)
        blinkt.clear()
        blinkt.set_brightness(0.1)
        blinkt.show()
        mn = self.debug("__init__", "starting ", PROGRAM_NAME, " v. ", PROGRAM_VERSION, "...")
        self.mqttClient = paho.mqtt.client.Client(client_id=PROGRAM_NAME) 
        self.debug(mn, "mqttClient: ", self.mqttClient)
        self.handlerRegistry = {}
        self.debug(mn, "created empty handler registry")
        self.debug(mn, "beginning handler registration...")
        self.registerHandler(Clear8Leds(self))
        self.registerHandler(Set8Leds(self))
        self.registerHandler(Random8Leds(self))
        self.debug(mn, "handler registration completed")
        self.mqttClient.on_connect = self.on_connect
        self.mqttClient.on_message = self.on_message
        self.debug(mn, "connecting to MQTT broker...")
        self.mqttClient.connect(LOCALHOST)
        self.debug(mn, "connected")
        self.mqttClient.loop_forever()
        
    def registerHandler(self, handler):
        inputTopic = handler.inputTopic
        self.handlerRegistry[inputTopic] = handler
        self.debug("registerHandler", inputTopic, " => ", handler)

    def on_connect(self, client, userdata, flags, rc):
        mn = self.debug("on_connnect", "client: ", client)
        self.debug(mn, "userdata: ", userdata)
        self.debug(mn, "flags: ", flags)
        self.debug(mn, "rc: ", rc)
        self.mqttClient.subscribe(TOPICS_FOR_SUBSCRIPTION) 
        self.debug(mn, "subscription completed")

    def on_message(self, client, userdata, message):
        mn = self.debug("on_message", "client: ", client)
        self.debug(mn, "userdata: ", userdata)
        self.debug(mn, "message: ", message)
        topic = message.topic
        self.debug(mn, "topic: ", topic)
        inputPayload = message.payload 
        if not inputPayload or inputPayload == None:
            inputPayload = ""
        if len(inputPayload) > 0:
            inputPayload = inputPayload.decode('utf-8')
        inputPayload = str(inputPayload).strip()
        self.debug(mn, "inputPayload: ", inputPayload)
        if (not topic.startswith("/")):
            topic = "/" + topic
        topicParts = topic.split("/")
        if len(topicParts) != 4:
           return
        handler = self.handlerRegistry.get(topic, None)
        if handler:
            args = handler.processInputPayload(inputPayload)
            if (args != None):
                outputPayload = handler.computeOutputPayload(args)
                outputTopic = handler.outputTopic
                if outputTopic != None and outputPayload != None:
                    self.mqttClient.publish(outputTopic, outputPayload)
                    
class AbstractTemplate(LoggableObject):
     
    def __init__(self, main, inputTopic, outputTopic=None):
        LoggableObject.__init__(self)
        self.main = main
        self.inputTopic = inputTopic
        mn = self.debug("__init__", "inputTopic: ", self.inputTopic)
        self.outputTopic = outputTopic
        self.debug(mn, "outputTopic: ", self.outputTopic)
        main.handlerRegistry[inputTopic] = self
        
    def processInputPayload(self, payload):
        return None
    
    def computeOutputPayload(self, *args):
        return None

class Abstract8Leds(AbstractTemplate):
     
    def __init__(self, main, detail):
        AbstractTemplate.__init__(self, main, "/" + TOPIC_ROOT_PART + "/" + TOPIC_8LEDS_PART + "/" + detail)

    def resetLights(self):
        blinkt.clear()
        blinkt.set_brightness(0.1)
        
class Clear8Leds(Abstract8Leds):
     
    def __init__(self, main):
        Abstract8Leds.__init__(self, main, "clear")
     
    def processInputPayload(self, payload):
        mn = self.debug("processInputPayload", "payload: ", payload)
        self.resetLights()
        blinkt.show()
 
class Set8Leds(Abstract8Leds):
     
    def __init__(self, main):
        Abstract8Leds.__init__(self, main, "set")
     
    def processInputPayload(self, payload):
        mn = self.debug("processInputPayload", "payload: ", payload)
        if payload:
            blinkt.clear()
            parts = payload.split()
            quadCount = (len(parts)-1) // 4
            brightness = text2floatClamp(parts[0],0.0,1.0)
            blinkt.set_brightness(brightness)
            index = 1
            for quad in range(quadCount):
                led = text2intClamp(parts[index], 0, 7)
                red = text2intClamp(parts[index+1], 0, 255)
                green = text2intClamp(parts[index+2], 0, 255)
                blue = text2intClamp(parts[index+3], 0, 255)
                index = index + 4
                blinkt.set_pixel(led,red, green, blue)
            blinkt.show() 

class Random8Leds(Abstract8Leds):
     
    def __init__(self, main):
        Abstract8Leds.__init__(self, main, "random")
     
    def processInputPayload(self, payload):
        mn = self.debug("processInputPayload", "payload: ", payload)
        self.resetLights()
        for led in range(8):
            red = random.randint(0,255)
            green = random.randint(0,255)
            blue = random.randint(0,255)
            blinkt.set_pixel(led,red, green, blue)
        blinkt.show() 

if __name__ == '__main__':
    Main()


    
